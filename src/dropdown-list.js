(function() {
    if (!AP.dropdownList) {
        return;
    }

    var dropdownSelectCallbacks = {};
    var dropdownHideCallbacks = {};

    var original_dropdownListConstructor = AP._hostModules.dropdownList.create;

    var iframeDropdownListConstructor = function() {
        var dropdownList = original_dropdownListConstructor();
        dropdownList.onSelect = function(cb) {
            dropdownSelectCallbacks[dropdownList._id] = cb;
        };
        dropdownList.onHide = function(cb) {
            dropdownHideCallbacks[dropdownList._id] = cb;
        };
        return dropdownList;
    };

    AP._hostModules.dropdownList.create = iframeDropdownListConstructor;
    AP.dropdownList.create = iframeDropdownListConstructor;

    AP.register({
        dropdown_list_select: function (event) {
            var callback = dropdownSelectCallbacks[event.id];
            if (callback) {
                callback.call({}, event.data);
            }
        },
        dropdown_list_hide: function (event) {
            var callback = dropdownHideCallbacks[event.id];
            if (callback) {
                callback.call({});
            }
        }
    });
})();