/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                             *
 *   (                                                                                         *
 *   )\ )                                       )       (                                      *
 *  (()/(     (         (      (          )  ( /(   (   )\ )                                   *
 *   /(_))   ))\ `  )   )(    ))\  (   ( /(  )\()) ))\ (()/(                                   *
 *  (_))_   /((_)/(/(  (()\  /((_) )\  )(_))(_))/ /((_) ((_))                                  *
 *   |   \ (_)) ((_)_\  ((_)(_))  ((_)((_)_ | |_ (_))   _| |                                   *
 *   | |) |/ -_)| '_ \)| '_|/ -_)/ _| / _` ||  _|/ -_)/ _` |                                   *
 *   |___/ \___|| .__/ |_|  \___|\__| \__,_| \__|\___|\__,_|                                   *
 *              |_|                                                                            *
 *                                                                                             *
 *  The code added to iframe.js coming from simple-xdm is for backwards compatibility only     *
 *  No new code may be added to plugin/* All modules must be defined in the host product       *
 *  For examples on how to extend ACJS please see https://bitbucket.org/atlassian/simple-xdm   *
 *                                                                                             *
 *        " No changes necessary in the iframe library when the host API changes "             *
 *                                                                                             *
 *  also see https://bitbucket.org/atlassian/atlassian-connect-js and host/*                   *
 *                                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

//map AP.env.getUser to AP.user.getUser for compatibility - same for getTimeZone
if(AP._hostModules.user && AP._hostModules.env) {
  Object.getOwnPropertyNames(AP._hostModules.user).forEach(function(method){
    AP[method] = AP.env[method] = AP._hostModules.env[method] = AP._hostModules.user[method];
  });
}
// AP.getLocation from AP.env.getLocation
// AP.sizeToParent from AP.env.sizeToParent
if(AP.env){
  AP.getLocation = AP._hostModules._globals.getLocation = AP._hostModules.env.getLocation;
  AP.sizeToParent = AP._hostModules._globals.sizeToParent = AP._hostModules.env.sizeToParent;
}

// support for dialog.isCloseOnEscape
if (AP._hostModules.dialog && AP._hostModules.Dialog) {
  function isCloseOnEscape(cb) {
    cb(!AP._data.options.preventDialogCloseOnEscape);
  }
  /**
   * Queries the value of the 'closeOnEscape' property.
   *
   * If this property is true then the dialog should close if ESC is pressed.
   * @memberOf module:Dialog
   * @method isCloseOnEscape
   * @param {Function} callback function to receive the 'closeOnEscape' value.
   * @noDemo
   * @example
   * AP.dialog.isCloseOnEscape(function(enabled){
   *   if(enabled){
   *     // close on escape is true
   *   }
   * });
   */
  AP._hostModules.dialog.isCloseOnEscape = AP._hostModules.Dialog.isCloseOnEscape = isCloseOnEscape;
  AP.dialog.isCloseOnEscape = AP.Dialog.isCloseOnEscape = isCloseOnEscape;
}

