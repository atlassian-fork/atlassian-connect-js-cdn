/* eslint-env node */
var gulp = require('gulp');
var concat = require('gulp-concat');
var fs = require('fs-extra');
var closureCompiler = require('google-closure-compiler').gulp();
var git = require('gulp-git');
var binHelper = require('./bin/bin-helper');
var exec = require('child_process').exec;
var karmaServer = require('karma').Server;
var replace = require('gulp-string-replace');
const FILE_LIST = [
  'node_modules/atlassian-connect-js/dist/iframe.js',
  'src/combined/iframe-insertion.js',
  'node_modules/atlassian-connect-js-history/dist/connect-plugin-history.js',
  'src/jira.js',
  'src/dropdown-list.js',
  'node_modules/atlassian-connect-js-request/dist/connect-plugin-request.js',
  'src/p2compat.js',
  'src/confluence.js',
];

var COMBINED_SRC = './src/combined/iframe-insertion.js';

const REMOTE_REPO = 'http://bitbucket.org/atlassian/connect-js-cdn.git';
var BUILD_DIST_DIR = './dist'; // NEVER CHANGE THIS IT WILL BREAK MICROS DEPLOYS OF OLD TAGS
var BUILD_MICROS_TAG_DIST_DIR = './microsdist';

function getTask(task) {
  return require('./gulp-tasks/' + task)(gulp);
}

function build() {
  gulp.src(FILE_LIST)
    .pipe(concat('all-debug.js'))
    .pipe(gulp.dest(BUILD_DIST_DIR + '/'));

  gulp.src(FILE_LIST)
      .pipe(closureCompiler({
          compilation_level: 'SIMPLE',
          // warning_level: 'VERBOSE',
          extra_annotation_name: ['noDemo', 'product'],
          jscomp_off: ['misplacedTypeAnnotation', 'nonStandardJsDocs'],
          rewrite_polyfills: 'false',
          language_in: 'ECMASCRIPT6',
          language_out: 'ECMASCRIPT5',
          charset: 'UTF-8',
          js_output_file: 'all.js'
        }))
      .pipe(gulp.dest(BUILD_DIST_DIR + '/'));

}

function buildMicros(){
  if(!fs.existsSync(BUILD_MICROS_TAG_DIST_DIR)){
    fs.mkdirSync(BUILD_MICROS_TAG_DIST_DIR);
  }

  git.tag(function(i, tags) {
    tags.forEach((tag) => {
      if(tag.length > 1) {
        createTagBuild(tag);
      }
    });

    fs.copySync(BUILD_DIST_DIR + '/all-debug.js', BUILD_MICROS_TAG_DIST_DIR + '/all-debug.js');
    fs.copySync(BUILD_DIST_DIR + '/all.js', BUILD_MICROS_TAG_DIST_DIR + '/all.js');
  });
};


function createTagBuild(tag){
  var tagDistDirectory = BUILD_MICROS_TAG_DIST_DIR + '/' + tag;
  if(!fs.existsSync(tagDistDirectory)){
    fs.mkdirSync(tagDistDirectory);
  }
  // eg: git --no-pager show v1:dist/all.js > dist/1/all.js
  exec('git --no-pager show ' + tag + ':' + binHelper.npmNormalize(BUILD_DIST_DIR + '/all.js'), function(error, stdout, stderr){
    fs.outputFileSync(binHelper.npmNormalize(tagDistDirectory + '/all.js'), stdout);
  });
  exec('git --no-pager show ' + tag + ':' + binHelper.npmNormalize(BUILD_DIST_DIR + '/all-debug.js'), function(error, stdout, stderr){
    fs.outputFileSync(binHelper.npmNormalize(tagDistDirectory + '/all-debug.js'), stdout);
  });
};

//ship a "combined.js" file that the product can consume for the host side of the bridge.
function buildCombined() {
    gulp.src(COMBINED_SRC)
    .pipe(concat('combined.js')) //use concat plugin as a way to rename the file
    .pipe(gulp.dest(BUILD_DIST_DIR + '/'));
}


gulp.task('build:micros', buildMicros);
gulp.task('build', ['build:alljs', 'build:combinedjs']);
gulp.task('build:alljs', build);
gulp.task('build:combinedjs', buildCombined);
gulp.task('karma', function (done) {
  new karmaServer({
    configFile: __dirname + '/karma.conf.js',
  }, done).start();
});
gulp.task('karma-ci', function (done) {
  new karmaServer({
    configFile: __dirname + '/karma.conf.js',
    singleRun:true
  }, done).start();
});

gulp.task('default', ['build']);